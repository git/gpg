# Carefully consider where you will clone this repository to,
# in order to distribute shares of your secret key, and configure these
# parameters.
# 
# Number of shares needed to regenerate the secret key.
SHARES_NEEDED=3
# Number of total shares to create.
SHARES_GENERATED=5

KEYFILES=$(shell find secring.gpg private-keys-v1.d/*.key 2>/dev/null)

restore:
	@for k in $(KEYFILES); do \
		git annex get $$k.??? || true; \
		if [ "`git annex find $$k.??? | wc -l`" -ge $(SHARES_NEEDED) ]; then \
			gfcombine -o $$k `git annex find $$k.???`; \
			echo "Regenerated $$k from shares!"; \
		else \
			echo "Failed to get enough shares to regenrate $$k"; \
		fi; \
	done

# Generate shares that can be recombined to generate the secret key files.
# These shares are added to git-annex, which allows you to easily
# git annex move individual shares to repositories.
shares:
	for k in $(KEYFILES); do \
		git rm $$k.???; \
		gfsplit -n 3 -m 5 $$k; \
		git annex add $$k.???; \
	done
	git commit -m "added shares"
